﻿using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework.Input;

namespace DalbinBlue.LudumDare40.MoreWorse
{
    internal class ControllerState : ControllerStateBase
    {
        [ControllerButton("Action", DefaultButton = Buttons.X, DefaultKey = Keys.Space)]
        public ControllerButtonState Action { get; set; }

        [ControllerButton("Exit", DefaultButton = Buttons.Back, DefaultKey = Keys.Escape)]
        public ControllerButtonState Exit { get; set; }
    }
}