﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dalbinblue.BaseGameLibrary.Audio;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Graphics.Camera;
using Dalbinblue.BaseGameLibrary.Graphics.Hud;
using Dalbinblue.BaseGameLibrary.Input;
using Dalbinblue.BaseGameLibrary.Physics;
using DalbinBlue.LudumDare40.MoreWorse.Actors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DalbinBlue.LudumDare40.MoreWorse
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameEngine : Game
    {
        private int _startLevelNumber = 1;
        public string[] LevelHints =
        {
            "Reach the checker goal to complete level.",
            "Sheep will follow you if you walk by them.",
            "You can't walk through sheep.  If you get surrounded, you'll have to restart the level.",
            "Some gates are locked and won't open until you collect enough sheep.",
            "Note that in some levels you don't need to collect all the sheep.",
            "Watch out for hungry wolves.  They will chase after sheep it in their line of sight.",
            "Wolves will track sheep over water, but not when there is a fence between.",
            "Boxes can be pushed, but can still block you if there is no room.",
            "Blocks will also block wolves.",
            "How did all the sheep get on these islands?",
            "You'll have to trick the wolf to get the sheep she guards.",
            "Why are these sheep blocking the way out?",
            "So many sheep!"
        };
        
        readonly GraphicsDeviceManager _graphics;
        private bool _gameComplete;

        // Main Rendering Components
        public RenderEngine RenderEngine { get; set; }
        public TransitionCamera Camera { get; set; }

        public const int TileWidth = 16;
        public const int TileHeight = 16;
        public const int ScreenWidthInTiles = 16;
        public const int ScreenHeightInTiles = 15;

        // Graphics Layers
        public Layer HudLayer { get; set; }
        public Layer ActorLayer { get; set; }
        public Layer BackgroundActorLayer { get; set; }
        public Layer BackgroundLayer { get; set; }
        public Layer UpperBackgroundLayer { get; set; }
        public RevealingTextDisplay TextDisplay { get; set; }

        // Assets
        public SpriteSheet SpriteSheet { get; set; }
        public SpriteFont Font { get; set; }

        // Controls
        private ControllerManager<ControllerState> ControllerManager { get; set; }

        // Sound
        public SoundManager SoundManager { get; set; }

        // Engine State
        public int CurrentLevelNumber { get; set; }
        public Point RespawnPoint { get; set; }

        // Game Objects
        public TileMap LevelMap { get; set; }
        public BasicTileCollisionMap MovementCollisionMap { get; set; }
        public BasicTileCollisionMap VisibilityCollisionMap { get; set; }
        public Player Player { get; set; }
        public List<ActorBase> Actors { get; set; }
        public int KeyCount { get; set; }


        public GameEngine()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void LoadContent()
        {
            LoadCommonAssets();
            InitializeGraphics();
            InitializeController();
            InitializeSound();

            LoadLevel(_startLevelNumber);
        }

        private void InitializeSound()
        {
            SoundManager = new SoundManager(Content);
            SoundManager.RegisterSoundEffect("Whine","Whine");
            SoundManager.RegisterSoundEffect("Wolf","Wolf");
            SoundManager.RegisterSoundEffect("Sheep","Sheep", new SoundEffectOptions {KeepSoundEffectInMemory = true, SoundEffectPlaybackMethod = SoundEffectPlaybackMethod.PlayIfNotAlreadyPlaying});
            SoundManager.RegisterMusic("Song","Song");
            SoundManager.PlayMusic("Song");
        }

        private void LoadCommonAssets()
        {
            SpriteSheet = Content.Load<SpriteSheet>("SpriteSheet");
            Font = Content.Load<SpriteFont>("Font");
        }


        private void InitializeController()
        {
            ControllerManager = new ControllerManager<ControllerState>();
        }

        private void InitializeGraphics()
        {
            RenderEngine = new RenderEngine(_graphics, TileWidth * ScreenWidthInTiles, TileHeight * ScreenHeightInTiles, true);

            Camera = new TransitionCamera
            {
                ViewPortWidth = TileWidth * ScreenWidthInTiles,
                ViewPortHeight = TileHeight * ScreenHeightInTiles
            };
            Camera.SetTarget(new Point(Camera.ViewPortWidth / 2, Camera.ViewPortHeight / 2));
            RenderEngine.Camera = Camera;

            BackgroundLayer = new Layer();
            RenderEngine.AddLayerToTop(BackgroundLayer);

            UpperBackgroundLayer = new Layer();
            RenderEngine.AddLayerToTop(UpperBackgroundLayer);

            BackgroundActorLayer = new Layer();
            RenderEngine.AddLayerToTop(BackgroundActorLayer);

            ActorLayer = new Layer();
            RenderEngine.AddLayerToTop(ActorLayer);

            HudLayer = new Layer() { IsHudLayer = true};
            RenderEngine.AddLayerToTop(HudLayer);

            TextDisplay = new RevealingTextDisplay(Font, TileWidth*ScreenWidthInTiles);
            TextDisplay.Position = new Vector2(0,(ScreenHeightInTiles - 2) * TileHeight);
            TextDisplay.Color = Color.White;
            HudLayer.Add(TextDisplay);
        }

        private void LoadLevel(int levelNumber)
        {
            BackgroundLayer.Clear();
            UpperBackgroundLayer.Clear();
            BackgroundActorLayer.Clear();
            ActorLayer.Clear();
            InLevelCompletionMode = false;
            KeyCount = 0;

            TextDisplay.SetTextToReveal(LevelHints[levelNumber-1]);

            CurrentLevelNumber = levelNumber;
            LevelMap = Content.Load<TileMap>($"Level{levelNumber}");

            var backgroundTileGrid = TileGrid.FromTileMapLayer(LevelMap, "Background", Content);
            BackgroundLayer.Add(backgroundTileGrid);

            var upperBackgroundTileGrid = TileGrid.FromTileMapLayer(LevelMap, "UpperBackground", Content);
            UpperBackgroundLayer.Add(upperBackgroundTileGrid);

            VisibilityCollisionMap = BasicTileCollisionMap.FromTileMapLayerAndTileIndex(LevelMap, "Collision", 0);
            var waterCollissionMap = BasicTileCollisionMap.FromTileMapLayerAndTileIndex(LevelMap, "Collision", 1);
            MovementCollisionMap = VisibilityCollisionMap.CombineWith(waterCollissionMap);

            var loadedActors = LoadActors(LevelMap);
            var playerActor = loadedActors.OfType<Player>().FirstOrDefault();
            if (playerActor != null)
            {
                Player = playerActor;
                playerActor.AddGraphicsToScene();
                loadedActors.Remove(playerActor);
                RespawnPoint = Player.Position.ToPoint();
            }

            Actors = new List<ActorBase>();
            foreach (var loadedActor in loadedActors)
            {
                AddActor(loadedActor);
            }
            Actors = loadedActors;
        }

        private List<ActorBase> LoadActors(TileMap levelMap)
        {
            var actors = new List<ActorBase>();
            var actorLayer = levelMap.ObjectGroups.First(og => og.Name == "Actors");

            foreach (var actorDefinition in actorLayer.Objects)
            {
                var tileIndex = actorDefinition.TileId;
                var matchingTileSet = levelMap.Tilesets
                    .Where(ts => ts.FirstTileId <= tileIndex)
                    .OrderByDescending(ts => ts.FirstTileId)
                    .First();

                var actorId = tileIndex - matchingTileSet.FirstTileId;
                var actorPosition = new Point(actorDefinition.X + 8, actorDefinition.Y - 1);
                ActorType actorType = (ActorType) actorId;

                switch (actorType)
                {
                    case ActorType.Player:
                        actors.Add(CreatePlayer(actorPosition));
                        break;
                    case ActorType.Sheep:
                        actors.Add(CreateSheep(actorPosition));
                        break;
                    case ActorType.Goal:
                        actors.Add(CreateGoal(actorPosition));
                        break;
                    case ActorType.Wolf:
                        actors.Add(CreateWolf(actorPosition));
                        break;
                    case ActorType.SheepGate1:
                        actors.Add(CreateSheepCountLock(actorPosition, 1));
                        break;
                    case ActorType.SheepGate2:
                        actors.Add(CreateSheepCountLock(actorPosition, 2));
                        break;
                    case ActorType.SheepGate3:
                        actors.Add(CreateSheepCountLock(actorPosition, 3));
                        break;
                    case ActorType.SheepGate4:
                        actors.Add(CreateSheepCountLock(actorPosition, 4));
                        break;
                    case ActorType.SheepGate5:
                        actors.Add(CreateSheepCountLock(actorPosition, 5));
                        break;
                    case ActorType.SheepGate6:
                        actors.Add(CreateSheepCountLock(actorPosition, 6));
                        break;
                    case ActorType.SheepGate7:
                        actors.Add(CreateSheepCountLock(actorPosition, 7));
                        break;
                    case ActorType.SheepGate8:
                        actors.Add(CreateSheepCountLock(actorPosition, 8));
                        break;
                    case ActorType.SheepGate9:
                        actors.Add(CreateSheepCountLock(actorPosition, 9));
                        break;
                    case ActorType.SheepGate10:
                        actors.Add(CreateSheepCountLock(actorPosition, 10));
                        break;
                    case ActorType.SheepGate11:
                        actors.Add(CreateSheepCountLock(actorPosition, 11));
                        break;
                    case ActorType.SheepGate12:
                        actors.Add(CreateSheepCountLock(actorPosition, 12));
                        break;
                    case ActorType.Box:
                        actors.Add(CreateBox(actorPosition));
                        break;
                    case ActorType.Key:
                        actors.Add(CreateKey(actorPosition));
                        break;
                    case ActorType.KeyLock:
                        actors.Add(CreateKeyLock(actorPosition));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return actors;
        }

        private ActorBase CreateKeyLock(Point actorPosition)
        {
            var keyLock = new KeyLock(this);
            keyLock.SetPosition(actorPosition);
            return keyLock;
        }

        private ActorBase CreateBox(Point actorPosition)
        {
            var box = new Box(this);
            box.SetPosition(actorPosition);
            return box;
        }

        private ActorBase CreateKey(Point actorPosition)
        {
            var key = new Key(this);
            key.SetPosition(actorPosition);
            return key;
        }

        private ActorBase CreateWolf(Point actorPosition)
        {
            var wolf = new Wolf(this);
            wolf.SetPosition(actorPosition);
            return wolf;
        }

        private ActorBase CreateSheepCountLock(Point actorPosition, int neededCount)
        {
            var sheepCountLock = new SheepCountLock(this, neededCount);
            sheepCountLock.SetPosition(actorPosition);
            return sheepCountLock;
        }

        private ActorBase CreateGoal(Point actorPosition)
        {
            var goal = new Goal(this);
            goal.SetPosition(actorPosition);
            return goal;
        }

        private ActorBase CreateSheep(Point actorPosition)
        {
            var sheep = new Sheep(this);
            sheep.SetPosition(actorPosition);
            return sheep;
        }

        private ActorBase CreatePlayer(Point actorPosition)
        {
            var player = new Player(this);
            player.SetPosition(actorPosition);
            return player;
        }

        public void AddActor(ActorBase actor)
        {
            actor.AddGraphicsToScene();
            Actors.Add(actor);
        }

        public void RemoveActor(ActorBase actor)
        {
            actor.RemoveGraphicsFromScene();
            Actors.Remove(actor);
        }

        protected override void Update(GameTime gameTime)
        {
            HandleInput(gameTime);
            UpdateActors(gameTime);
            if (InLevelCompletionMode)
            {
                UpdateLevelCompletion(gameTime);
            }
            RenderEngine.Update(gameTime);
            base.Update(gameTime);
        }

        private void UpdateLevelCompletion(GameTime gameTime)
        {
            var lastSheep = Actors.OfType<Sheep>().FirstOrDefault(s => s.IsLastSheepInFlock);

            if (lastSheep != null)
            {
                if (!lastSheep.IsMoving && lastSheep.TargetPosition == Player.TargetPosition)
                {
                    TriggerNextLevel();
                }
            }
            else
            {
                TriggerNextLevel();
            }
        }

        private void TriggerNextLevel()
        {
            if (CurrentLevelNumber == LevelHints.Length)
            {
                if (!_gameComplete)
                {
                    TextDisplay.SetTextToReveal("All levels complete!  Congrats!   Made by DalbinBlue in 48 hours for Ludum Dare 40.");
                }
                _gameComplete = true;
            } 
            else
            {
                LoadLevel(CurrentLevelNumber + 1);
            }
        }

        private void UpdateActors(GameTime gameTime)
        {
            Player.Update(gameTime);

            var actorCopy = new List<ActorBase>(Actors);
            foreach (var actor in actorCopy)
            {
                actor.Update(gameTime);
            }

            actorCopy = new List<ActorBase>(Actors);

            var playerTargetPosition = Player.TargetPosition;
            for (int i = 0; i < actorCopy.Count; i++)
            {
                var actor = actorCopy[i];
                var actorTargetPosition = actor.TargetPosition;
                if (actorTargetPosition == playerTargetPosition)
                {
                    actor.HandleCollisionWithPlayer();
                }

                for (int j = 0; j < actorCopy.Count; j++)
                {
                    if (i == j) continue;
                    var otherActor = actorCopy[j];
                    var otherActorTargetPosition = otherActor.TargetPosition;
                    if (actorTargetPosition == otherActorTargetPosition)
                    {
                        actor.HandleCollisionWithActor(otherActor);
                    }
                }
            }

        }

        private void HandleInput(GameTime gameTime)
        {
            var controllerState = ControllerManager.Update(gameTime);

            if (controllerState.Exit.IsJustPressed)
            {
                Exit();
            }

            if (controllerState.Action.IsJustPressed)
            {
                //TODO Bark?
            }

            if (!Player.IsMoving)
            {
                if (controllerState.Up.IsDown)
                {
                    Player.TryMove(CardinalDirection.Up);
                }
                else if (controllerState.Down.IsDown)
                {
                    Player.TryMove(CardinalDirection.Down);
                }
                else if (controllerState.Left.IsDown)
                {
                    Player.TryMove(CardinalDirection.Left);
                }
                else if (controllerState.Right.IsDown)
                {
                    Player.TryMove(CardinalDirection.Right);
                }
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            RenderEngine.Draw(gameTime);
            base.Draw(gameTime);
        }

        public void SignalActorsToPerformMove()
        {
            var actorCopy = new List<ActorBase>(Actors.Where(a => !a.IsEnemy));
            foreach (var actor in actorCopy)
            {
                actor.PerformMove();
            }
            actorCopy = new List<ActorBase>(Actors.Where(a => a.IsEnemy));
            foreach (var actor in actorCopy)
            {
                actor.PerformMove();
            }
        }

        public void TryAddSheep()
        {
            var sheepOnLevel = Actors.OfType<Sheep>().ToList();

            var continueTesting = true;

            while (continueTesting)
            {
                continueTesting = false;
                foreach (var sheep in sheepOnLevel)
                {
                    if (sheep.TestAddToFlock())
                    {
                        continueTesting = true;
                    }
                }
            }
        }

        public void RestartLevel()
        {
            LoadLevel(CurrentLevelNumber);
        }

        public void TriggerLevelComplete()
        {
            InLevelCompletionMode = true;
        }

        public bool InLevelCompletionMode { get; set; }
    }
}
