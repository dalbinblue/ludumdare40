﻿using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class Box : ActorBase
    {
        public Box(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "Box", engine.Content);
            Sprite.SetAnimation("Idle");
        }

        public override bool BlocksMovement => true;
        public override bool BlocksVisibility => true;

        public override bool BlocksPlayerMovement
        {
            get
            {
                var playerPosition = Engine.Player.TilePosition;

                var direction = CardinalDirection.None;
                if (playerPosition.X > TilePosition.X)
                {
                    direction = CardinalDirection.Left;
                }
                else if (playerPosition.X < TilePosition.X)
                {
                    direction = CardinalDirection.Right;
                }
                else if (playerPosition.Y > TilePosition.Y)
                {
                    direction = CardinalDirection.Up;
                }
                else if (playerPosition.Y < TilePosition.Y)
                {
                    direction = CardinalDirection.Down;
                }

                return !CanAnObjectMoveInDirection(TilePosition, direction);
            }
        }

        public override void PerformMove()
        {
            if (Engine.Player.TargetPosition == TargetPosition)
            {
                Point movementOffset = GetMovementOffsetForDirection(Engine.Player.LastMovedDirection);
                TargetPosition = Position.ToPoint().Shift(movementOffset);
            }
        }
    }
}