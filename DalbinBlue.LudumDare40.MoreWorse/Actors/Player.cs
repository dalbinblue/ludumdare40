﻿using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class Player : ActorBase
    {
        public Sheep FirstSheepInFlock { get; set; }
        public CardinalDirection LastMovedDirection = CardinalDirection.None;

        public Player(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "Player", engine.Content);
            Sprite.SetAnimation("Idle");
            Sprite.SpriteAnimationEnded += Sprite_SpriteAnimationEnded;
        }

        private void Sprite_SpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            if (args.AnimationName == "Die")
            {
                Engine.RestartLevel();
            }
        }

        public bool IsDying => Sprite.CurrentAnimationName == "Die";

        public void TryMove(CardinalDirection direction)
        {
            if (IsMoving) return;
            if (IsDying) return;
            if (Engine.InLevelCompletionMode) return;

            if (CanAnObjectMoveInDirection(TilePosition, direction))
            {
                Point movementOffset = GetMovementOffsetForDirection(direction);
                TargetPosition = Position.ToPoint().Shift(movementOffset);
                Sprite.SetAnimation("Walk");
                if (direction == CardinalDirection.Left)
                {
                    Sprite.FlipHorizontally = true;
                }
                if (direction == CardinalDirection.Right)
                {
                    Sprite.FlipHorizontally = false;
                }
                LastMovedDirection = direction;
                FirstSheepInFlock?.FollowTo(Position.ToPoint());
                Engine.TryAddSheep();
                Engine.SignalActorsToPerformMove();
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving && !IsDying)
            {
                Sprite.SetAnimation("Idle");
            }

            if (Engine.InLevelCompletionMode)
            {
                FirstSheepInFlock?.FollowTo(Position.ToPoint());
            }

            base.Update(gameTime);
        }

        protected override void HandleMotionComplete()
        {
            if (IsDying) return;
            if (IsSurrounded())
            {
                Engine.SoundManager.PlaySoundEffect("Whine");
                Kill();
            }
        }

        public override bool BlocksMovement => true;

        public void Kill()
        {
            Sprite.SetAnimation("Die");
        }
    }
}
