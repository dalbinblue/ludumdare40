﻿using System.Linq;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class SheepCountLock : ActorBase {
        public int NeededCount { get; }

        public SheepCountLock(GameEngine engine, int neededCount) : base(engine)
        {
            NeededCount = neededCount;
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "CountBlock", engine.Content);
            Sprite.SetAnimation($"Idle{NeededCount}");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;
        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            if (args.AnimationName == "Opening")
            {
                Engine.RemoveActor(this);
            }
        }

        public override bool BlocksMovement => true;
        public override bool BlocksPlayerMovement => true;
        public override bool BlocksVisibility => true;

        public override void Update(GameTime gameTime)
        {
            if (Sprite.CurrentAnimationName == "Opening") return;

            var sheepCount = Engine.Actors.OfType<Sheep>().Count(s => s.IsInFlock || s.IsWaitingForLastSheepInFlock);
            if (sheepCount >= NeededCount)
            {
                Sprite.SetAnimation("Opening");
            }

            base.Update(gameTime);
        }
    }
}