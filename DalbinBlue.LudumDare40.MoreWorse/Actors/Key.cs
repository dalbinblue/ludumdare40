﻿using Dalbinblue.BaseGameLibrary.Graphics;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class Key : ActorBase
    {
        public Key(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "Key", engine.Content);
            Sprite.SetAnimation("Idle");
        }

        public override void HandleCollisionWithPlayer()
        {
            Engine.RemoveActor(this);
            Engine.KeyCount += 1;
        }
    }
}