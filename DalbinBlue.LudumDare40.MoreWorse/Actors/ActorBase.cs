﻿using System;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class ActorBase
    {
        protected ActorBase(GameEngine engine)
        {
            Engine = engine;
            Speed = 1.8f;
            FacingDirection = CardinalDirection.Left;
        }

        protected float Speed { get; set; }
        protected AnimatedSprite Sprite { get; set; }
        protected GameEngine Engine { get; set; }
        public CardinalDirection FacingDirection { get; protected set; }
        public virtual bool IsEnemy => false;

        public virtual bool BlocksPlayerMovement => BlocksMovement;
        public virtual bool BlocksMovement => false;
        public virtual bool BlocksVisibility => false;
        
        public virtual Vector2 Position
        {
            get { return Sprite.Position; }
            private set { Sprite.Position = value; }
        }

        public virtual Point TargetPosition { get; set; }

        public virtual Point TilePosition => new Point((int)Position.X / 16, ((int)Position.Y - 8) / 16);

        public virtual Point TargetTilePosition => new Point(TargetPosition.X / 16, (TargetPosition.Y - 8) / 16);


        public bool IsMoving => TargetPosition != Position.ToPoint();

        public virtual void SetPosition(Point newPosition)
        {
            Sprite.Position = newPosition.ToVector();
            TargetPosition = newPosition;
        }

        public virtual void Update(GameTime gameTime)
        {
            UpdatePosition(gameTime);
        }

        private void UpdatePosition(GameTime gameTime)
        {
            var previouslyMoving = IsMoving;
            
            if (Position.X > TargetPosition.X)
            {
                Position = new Vector2(Math.Max(Position.X - Speed, TargetPosition.X), Position.Y);
            }
            else if (Position.X < TargetPosition.X)
            {
                Position = new Vector2(Math.Min(Position.X + Speed, TargetPosition.X), Position.Y);
            }

            if (Position.Y > TargetPosition.Y)
            {
                Position = new Vector2(Position.X, Math.Max(Position.Y - Speed, TargetPosition.Y));
            }
            else if (Position.Y < TargetPosition.Y)
            {
                Position = new Vector2(Position.X, Math.Min(Position.Y + Speed, TargetPosition.Y));
            }

            if (previouslyMoving && !IsMoving)
            {
                HandleMotionComplete();
            }
        }

        protected virtual void HandleMotionComplete()
        {
            // DO NOTHING
        }

        public virtual void RemoveGraphicsFromScene()
        {
            Engine.ActorLayer.Remove(Sprite);
        }

        public virtual void AddGraphicsToScene()
        {
            Engine.ActorLayer.Add(Sprite);
        }

        public Point GetMovementOffsetForDirection(CardinalDirection direction)
        {
            switch (direction)
            {
                case CardinalDirection.Up:
                    return new Point(0, -16);
                case CardinalDirection.Down:
                    return new Point(0, 16);
                case CardinalDirection.Left:
                    return new Point(-16, 0);
                case CardinalDirection.Right:
                    return new Point(16, 0);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction));
            }
        }

        protected bool CanAnObjectMoveInDirection(Point tilePosition, CardinalDirection direction)
        {
            Point targetPosition;

            switch (direction)
            {
                case CardinalDirection.Up:
                    targetPosition = tilePosition.Shift(0, -1);
                    break;
                case CardinalDirection.Down:
                    targetPosition = tilePosition.Shift(0, 1);
                    break;
                case CardinalDirection.Left:
                    targetPosition = tilePosition.Shift(-1, 0);
                    break;
                case CardinalDirection.Right:
                    targetPosition = tilePosition.Shift(1, 0);
                    break;
                case CardinalDirection.None:
                    targetPosition = tilePosition;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction));
            }

            if (Engine.MovementCollisionMap.IsObstacleAtTilePosition(targetPosition))
            {
                return false;
            }

            var currentActorIsPlayer = this is Player;
            foreach (var actor in Engine.Actors)
            {
                if (!(actor.TargetTilePosition == targetPosition)) continue;

                if (currentActorIsPlayer)
                {
                    if (actor.BlocksPlayerMovement)
                    {
                        return false;
                    }
                }
                else if (actor.BlocksMovement)
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsSurrounded()
        {
            return !CanAnObjectMoveInDirection(TilePosition, CardinalDirection.Down) &&
                   !CanAnObjectMoveInDirection(TilePosition, CardinalDirection.Up) &&
                   !CanAnObjectMoveInDirection(TilePosition, CardinalDirection.Left) &&
                   !CanAnObjectMoveInDirection(TilePosition, CardinalDirection.Right);
        }

        public virtual Rectangle GetCollisionBounds()
        {
            return Sprite.GetCollisionBounds();
        }

        public virtual void HandleCollisionWithPlayer()
        {
            // DO NOTHING
        }
        
        public virtual void HandleCollisionWithActor(ActorBase otherActor)
        {
            // DO NOTHING
        }

        public virtual void PerformMove()
        {
            // DO NOTHING
        }

        protected bool IsAdjacentTo(ActorBase other)
        {
            var otherPosition = other.TargetTilePosition;
            return otherPosition == TilePosition.Shift(-1, 0) ||
                   otherPosition == TilePosition.Shift(1, 0) ||
                   otherPosition == TilePosition.Shift(0, -1) ||
                   otherPosition == TilePosition.Shift(0, 1);
        }
    }
}