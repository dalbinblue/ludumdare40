﻿using System;
using System.Linq;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Audio;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class Wolf : ActorBase
    {
        public Wolf(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "Wolf", engine.Content);
            Sprite.SetAnimation("Idle");
        }

        public override bool IsEnemy => true;
        public override bool BlocksPlayerMovement => true;
        

        public override void PerformMove()
        {
            var sheepActors = Engine.Actors.OfType<Sheep>();

            Sheep bestSheep = null;
            int bestDistance = int.MaxValue;
            foreach (var sheepActor in sheepActors)
            {
                if (CanSee(sheepActor) && CanMoveTo(GetDirectionTo(sheepActor)))
                {
                    var currentSheepDistance = GetTileDistanceTo(sheepActor);
                    if (currentSheepDistance < bestDistance)
                    {
                        bestSheep = sheepActor;
                        bestDistance = currentSheepDistance;
                    }
                }
            }

            if (bestSheep != null)
            {
                MoveTowards(bestSheep);
            }
        }

        private CardinalDirection GetDirectionTo(ActorBase actor)
        {
            var targetTilePosition = actor.TargetTilePosition;
            var xOffset = targetTilePosition.X - TilePosition.X;
            var yOffset = targetTilePosition.Y - TilePosition.Y;

            if (xOffset < 0)
            {
                return CardinalDirection.Left;
            }
            if (xOffset > 0)
            {
                return CardinalDirection.Right;
            }

            if (yOffset < 0)
            {
                return CardinalDirection.Up;
            }
            if (yOffset > 0)
            {
                return CardinalDirection.Down;
            }
            return CardinalDirection.None;
        }

        private void MoveTowards(ActorBase actor)
        {
            var targetTilePosition = actor.TargetTilePosition;
            var xOffset = targetTilePosition.X - TilePosition.X;
            var yOffset = targetTilePosition.Y - TilePosition.Y;

            if (xOffset < 0) 
            {
                TryMove(CardinalDirection.Left);
            }
            if (xOffset > 0)
            {
                TryMove(CardinalDirection.Right);
            }

            if (yOffset < 0)
            {
                TryMove(CardinalDirection.Up);
            }
            if (yOffset > 0)
            {
                TryMove(CardinalDirection.Down);
            }
        }

        private void TryMove(CardinalDirection direction)
        {
            if (CanMoveTo(direction))
            {
                Point movementOffset = GetMovementOffsetForDirection(direction);
                TargetPosition = Position.ToPoint().Shift(movementOffset);
                Sprite.SetAnimation("Walk");
                if (direction == CardinalDirection.Left)
                {
                    Sprite.FlipHorizontally = true;
                }
                if (direction == CardinalDirection.Right)
                {
                    Sprite.FlipHorizontally = false;
                }
            }
        }

        private bool CanMoveTo(CardinalDirection direction)
        {
            if (CanAnObjectMoveInDirection(TilePosition, direction)) return true;

            var tileOffset = GetTileOffsetForDirection(direction);
            var targetTilePosition = TilePosition.Shift(tileOffset);
            return Engine.Actors.OfType<Sheep>().Any(s => s.TilePosition == targetTilePosition);
        }

        private Point GetTileOffsetForDirection(CardinalDirection direction)
        {
            switch (direction)
            {
                case CardinalDirection.None:
                    return Point.Zero;
                case CardinalDirection.Up:
                    return new Point(0, -1);
                case CardinalDirection.Down:
                    return new Point(0, 1);
                case CardinalDirection.Left:
                    return new Point(-1, 0);
                case CardinalDirection.Right:
                    return new Point(1, 0);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        private int GetTileDistanceTo(ActorBase actor)
        {
            var targetTilePosition = actor.TargetTilePosition;
            var xOffset = Math.Abs(targetTilePosition.X - TilePosition.X);
            var yOffset = Math.Abs(targetTilePosition.Y - TilePosition.Y);
            return xOffset + yOffset;
        }

        private bool CanSee(ActorBase actor)
        {
            var targetTilePosition = actor.TargetTilePosition;
            var xOffset = targetTilePosition.X - TilePosition.X;
            var yOffset = targetTilePosition.Y - TilePosition.Y;

            if (xOffset != 0 && yOffset != 0) return false;

            if (xOffset != 0)
            {
                int initialX = Math.Min(TilePosition.X, targetTilePosition.X);
                int finalX = Math.Max(TilePosition.X, targetTilePosition.X);

                for (int x = initialX; x <= finalX; x++)
                {
                    if (Engine.VisibilityCollisionMap.IsObstacleAtTilePosition(new Point(x, TilePosition.Y)) || IsVisibilityBlockingObjectAt(x,TilePosition.Y))
                    {
                        return false;
                    }
                }
            }

            if (yOffset != 0)
            {
                int initialY = Math.Min(TilePosition.Y, targetTilePosition.Y);
                int finalY = Math.Max(TilePosition.Y, targetTilePosition.Y);

                for (int y = initialY; y <= finalY; y++)
                {
                    if (Engine.VisibilityCollisionMap.IsObstacleAtTilePosition(new Point(TilePosition.X, y)) || IsVisibilityBlockingObjectAt(TilePosition.X, y))
                    {
                        return false;
                    }
                }
            }


            return true;
        }

        private bool IsVisibilityBlockingObjectAt(int x, int y)
        {
            var targetPoint = new Point(x,y);
            return Engine.Actors.Any(a => a.TargetTilePosition == targetPoint && a.BlocksVisibility);
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving && Sprite.CurrentAnimationName == "Walk")
            {
                Sprite.SetAnimation("Idle");
            }
            base.Update(gameTime);
        }

        public override bool BlocksMovement => true;

        public override void HandleCollisionWithActor(ActorBase otherActor)
        {
            if (otherActor is Sheep)
            {
                Engine.RemoveActor(otherActor);
                Sprite.SetAnimation("Attack");
                Engine.SoundManager.PlaySoundEffect("Wolf");
                Engine.Player.Kill();
            }
            base.HandleCollisionWithActor(otherActor);
        }
    }
}