﻿using Dalbinblue.BaseGameLibrary.Graphics;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class KeyLock : ActorBase
    {
        public KeyLock(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "KeyLock", engine.Content);
            Sprite.SetAnimation("Idle");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;
        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            if (args.AnimationName == "Opening")
            {
                Engine.RemoveActor(this);
            }
        }

        public override bool BlocksMovement => true;

        public override bool BlocksPlayerMovement => Engine.KeyCount == 0;

        public override bool BlocksVisibility => true;

        public override void HandleCollisionWithPlayer()
        {
            if (Sprite.CurrentAnimationName == "Opening") return;
            Engine.KeyCount -= 1;
            Sprite.SetAnimation("Opening");
        }
    }
}