﻿using System;
using Dalbinblue.BaseGameLibrary.Graphics;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class Goal : ActorBase
    {
        public Goal(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "Goal", engine.Content);
            Sprite.SetAnimation("Idle");
        }

        public override void RemoveGraphicsFromScene()
        {
            Engine.BackgroundActorLayer.Remove(Sprite);
        }

        public override void AddGraphicsToScene()
        {
            Engine.BackgroundActorLayer.Add(Sprite);
        }

        public override void HandleCollisionWithPlayer()
        {
            Engine.TriggerLevelComplete();
        }
    }
}