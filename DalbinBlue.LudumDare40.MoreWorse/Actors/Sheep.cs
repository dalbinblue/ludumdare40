﻿using System.Linq;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare40.MoreWorse.Actors
{
    public class Sheep : ActorBase
    {
        public Sheep(GameEngine engine) : base(engine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(engine.SpriteSheet, "Sheep", engine.Content);
            Sprite.SetAnimation("Idle");
        }

        public override bool BlocksMovement => true;

        public bool IsInFlock { get; private set; } = false;

        public bool IsWaitingForLastSheepInFlock { get; private set; }

        public Sheep NextSheepInFlock { get; private set; }

        public bool IsLastSheepInFlock => IsInFlock && (NextSheepInFlock == null);

        public bool TestAddToFlock()
        {
            if (IsInFlock) return false;
            if (IsWaitingForLastSheepInFlock)
            {
                var lastSheep = Engine.Actors.OfType<Sheep>().FirstOrDefault(s => s.IsLastSheepInFlock);
                if (lastSheep != null && IsAdjacentTo(lastSheep))
                {
                    lastSheep.NextSheepInFlock = this;
                    IsWaitingForLastSheepInFlock = false;
                    IsInFlock = true;
                    return true;
                }
            }
            else
            {
                var player = Engine.Player;
                if (IsAdjacentTo(player))
                {
                    if (player.FirstSheepInFlock == null)
                    {
                        player.FirstSheepInFlock = this;
                        IsInFlock = true;
                        Sprite.SetAnimation("JoinFlock");
                        Engine.SoundManager.PlaySoundEffect("Sheep");
                        return true;
                    }
                    IsWaitingForLastSheepInFlock = true;
                    Sprite.SetAnimation("JoinFlock");
                    Engine.SoundManager.PlaySoundEffect("Sheep");
                    return true;
                }
                if (Engine.Actors.OfType<Sheep>().Any(s => (s.IsWaitingForLastSheepInFlock || s.IsInFlock) && IsAdjacentTo(s)))
                {
                    IsWaitingForLastSheepInFlock = true;
                    Sprite.SetAnimation("JoinFlock");
                    Engine.SoundManager.PlaySoundEffect("Sheep");
                    return true;
                }
            }
            return false;
        }

        public void FollowTo(Point targetPosition)
        {
            NextSheepInFlock?.FollowTo(TargetPosition);

            if (targetPosition.X > TargetPosition.X)
            {
                Sprite.FlipHorizontally = false;
            }
            else if (targetPosition.X < TargetPosition.X)
            {
                Sprite.FlipHorizontally = true;
            }

            TargetPosition = targetPosition;
            Sprite.SetAnimation("Walk");
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving && Sprite.CurrentAnimationName != "JoinFlock")
            {
                if (IsInFlock || IsWaitingForLastSheepInFlock)
                {
                    Sprite.SetAnimation("IdleInFlock");
                }
                else
                {
                    Sprite.SetAnimation("Idle");
                }
            }
            base.Update(gameTime);
        }
    }
}