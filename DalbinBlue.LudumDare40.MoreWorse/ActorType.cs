﻿namespace DalbinBlue.LudumDare40.MoreWorse
{
    internal enum ActorType
    {
        Player,
        Sheep,
        Wolf,
        Goal,
        SheepGate1,
        SheepGate2,
        SheepGate3,
        SheepGate4,
        SheepGate5,
        SheepGate6,
        SheepGate7,
        SheepGate8,
        SheepGate9,
        SheepGate10,
        SheepGate11,
        SheepGate12,
        Box,
        Key,
        KeyLock
    }
}