﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Scripting
{
  public class ScriptTimer
  {
    private List<TimerState> _timers = new List<TimerState>();

    public ScriptState Wait(double seconds)
    {
      var scriptState = new ScriptState { IsComplete = false };
      _timers.Add(new TimerState { ScriptState = scriptState, RemainingSeconds = seconds });
      return scriptState;
    }

    public void Update(GameTime gameTime)
    {
      var elapsedTime = gameTime.ElapsedGameTime.TotalSeconds;

      for (int i = _timers.Count - 1; i >= 0; i--)
      {
        var state = _timers[i];
        state.RemainingSeconds -= elapsedTime;
        if (state.RemainingSeconds <= 0.0)
        {
          state.ScriptState.IsComplete = true;
          _timers.RemoveAt(i);
        }
      }
    }

    private class TimerState
    {
      public ScriptState ScriptState { get; set; }
      public double RemainingSeconds { get; set; }
    }
  }
}
