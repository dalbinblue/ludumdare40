﻿using System;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Microsoft.Xna.Framework;
using System.Linq;

namespace Dalbinblue.BaseGameLibrary.Physics {


  public interface ICollisionMap {
    bool IsObstacleAtTilePosition(Point tilePosition);
    TileMapCollisionResult TestCollision(Rectangle objectBounds, Point objectMovementOffset, bool onGroundPreviously);
  }

  public class BasicTileCollisionMap : ICollisionMap {
    public bool[][] ObstacleMap { get; private set; }
    public bool[][] PlatformMap { get; private set; }

    public int WidthInTiles { get; protected set; }
    public int HeightInTiles { get; protected set; }
    public int TileWidth { get; protected set; }
    public int TileHeight { get; protected set; }
    public Point Position { get; set; }

    public BasicTileCollisionMap(int widthInTiles, int heightInTiles, int tileWidth, int tileHeight) {
      WidthInTiles = widthInTiles;
      HeightInTiles = heightInTiles;
      TileWidth = tileWidth;
      TileHeight = tileHeight;
      Position = new Point(0, 0);
      ObstacleMap = new bool[widthInTiles][];
      PlatformMap = new bool[widthInTiles][];
      for (int i = 0; i < widthInTiles; i++)
      {
        ObstacleMap[i] = new bool[heightInTiles];
        PlatformMap[i] = new bool[heightInTiles];
      }
    }

    public BasicTileCollisionMap CombineWith(BasicTileCollisionMap other) {
      if ((WidthInTiles != other.WidthInTiles) ||
          (HeightInTiles != other.HeightInTiles) ||
          (TileWidth != other.TileWidth) ||
          (TileHeight != other.TileHeight)) {
        throw new ArgumentException("Dimensions must be the same");
      }

      BasicTileCollisionMap newMap = new BasicTileCollisionMap(WidthInTiles,HeightInTiles, TileWidth, TileHeight);

      for (int x = 0; x < WidthInTiles; x++) {
        for (int y = 0; y < HeightInTiles; y++) {
          newMap.ObstacleMap[x][y] = ObstacleMap[x][y] || other.ObstacleMap[x][y];
          newMap.PlatformMap[x][y] = !newMap.ObstacleMap[x][y] && (PlatformMap[x][y] || other.PlatformMap[x][y]);
        }
      }

      return newMap;
    }

    public BasicTileCollisionMap CombineWithLadderCollisionMap(BasicTileCollisionMap ladderCollision)
    {
        if ((WidthInTiles != ladderCollision.WidthInTiles) ||
            (HeightInTiles != ladderCollision.HeightInTiles) ||
            (TileWidth != ladderCollision.TileWidth) ||
            (TileHeight != ladderCollision.TileHeight))
        {
            throw new ArgumentException("Dimensions must be the same");
        }

        BasicTileCollisionMap newMap = new BasicTileCollisionMap(WidthInTiles, HeightInTiles, TileWidth, TileHeight);

        for (int x = 0; x < WidthInTiles; x++)
        {
            for (int y = 0; y < HeightInTiles; y++)
            {
                newMap.ObstacleMap[x][y] = ObstacleMap[x][y] || 
                    (ladderCollision.ObstacleMap[x][y] && y > 0 && !ladderCollision.ObstacleMap[x][y-1]);
            }
        }

        return newMap;
    }

    public BasicTileCollisionMap CreateExcludeMap(BasicTileCollisionMap other)
    {
      if ((WidthInTiles != other.WidthInTiles) ||
          (HeightInTiles != other.HeightInTiles) ||
          (TileWidth != other.TileWidth) ||
          (TileHeight != other.TileHeight))
      {
        throw new ArgumentException("Dimensions must be the same");
      }

      BasicTileCollisionMap newMap = new BasicTileCollisionMap(WidthInTiles, HeightInTiles, TileWidth, TileHeight);

      for (int x = 0; x < WidthInTiles; x++)
      {
        for (int y = 0; y < HeightInTiles; y++)
        {
          newMap.ObstacleMap[x][y] = ObstacleMap[x][y] && !other.ObstacleMap[x][y];
        }
      }

      return newMap;
    }


    public BasicTileCollisionMap CreateInvertMap() {
      BasicTileCollisionMap newMap = new BasicTileCollisionMap(WidthInTiles, HeightInTiles, TileWidth, TileHeight);

      for (int x = 0; x < WidthInTiles; x++)
      {
        for (int y = 0; y < HeightInTiles; y++) {
          newMap.ObstacleMap[x][y] = !ObstacleMap[x][y];
        }
      }

      return newMap;      
    }



    public static BasicTileCollisionMap FromTileMapLayer(TileMap tileMap, int layerIndex) {
      var map = new BasicTileCollisionMap(tileMap.WidthInTiles, tileMap.HeightInTiles, tileMap.TileWidth, tileMap.TileHeight);

      

      var layer = tileMap.Layers[layerIndex];

      for (int x = 0; x < tileMap.WidthInTiles; x++) {
        for (int y = 0; y < tileMap.HeightInTiles; y++)
        {
          long index = layer.LayerTiles[x][y];

          if (index == 0) continue;
          index &= 0xFFFF;
          var tileSet = tileMap.Tilesets.Where(ts => ts.FirstTileId <= index).OrderByDescending(ts => ts.FirstTileId).First();
          index -= tileSet.FirstTileId;

          if (index == 0)
          {
            map.ObstacleMap[x][y] = true;              
          }
          else if (index == 1)
          {
            map.PlatformMap[x][y] = true;
          }
        }
      }

      return map;
    }

      public static BasicTileCollisionMap FromTileMapLayerAndTileIndex(TileMap tileMap, string layerName, int tileIndex)
      {
          return FromTileMapLayerAndTileIndex(tileMap, tileMap.GetLayerIndexByName(layerName), tileIndex);
      }


      public static BasicTileCollisionMap FromTileMapLayerAndTileIndex(TileMap tileMap, int layerIndex, int tileIndex)
      {
          var map = new BasicTileCollisionMap(tileMap.WidthInTiles, tileMap.HeightInTiles, tileMap.TileWidth, tileMap.TileHeight);

          var layer = tileMap.Layers[layerIndex];

          for (int x = 0; x < tileMap.WidthInTiles; x++)
          {
              for (int y = 0; y < tileMap.HeightInTiles; y++)
              {
                  long index = layer.LayerTiles[x][y];

                  if (index == 0) continue;
                  index &= 0xFFFF;
                  var tileSet = tileMap.Tilesets.Where(ts => ts.FirstTileId <= index).OrderByDescending(ts => ts.FirstTileId).First();
                  index -= tileSet.FirstTileId;

                  if (index == tileIndex)
                  {
                      map.ObstacleMap[x][y] = true;
                  }
              }
          }

          return map;
      }

        public bool IsObstacleAtTilePosition(Point tilePosition) {
      if (tilePosition.X < 0 || 
        tilePosition.X >= WidthInTiles || 
        tilePosition.Y < 0 || 
        tilePosition.Y >= HeightInTiles)
        return true;
      return ObstacleMap[tilePosition.X][tilePosition.Y];
    }

    public bool TestNonMovingCollision(Rectangle objectBounds) {
      var candidateTileBounds = GetCandidateTileCoordinateBounds(objectBounds);
      for (int x = candidateTileBounds.Left; x <= candidateTileBounds.Right; x++) {
        for (int y = candidateTileBounds.Top; y <= candidateTileBounds.Bottom; y++) {
          if (IsObstacleAtTilePosition(new Point(x, y))) return true;
        }
      }
      return false;
    }

    public TileMapCollisionResult TestCollision(Rectangle objectBounds, Point objectMovementOffset, bool onGroundPreviously)
    {
      var originalOffset = objectMovementOffset;

      // Get the entire rectange that the object sweeps through
      var newTargetBounds = objectBounds.Shift(objectMovementOffset);
      var sweepBounds = Rectangle.Union(objectBounds, newTargetBounds);

      // Determine the range of x and y indices that the player is moving through
      Rectangle candidateTileIndexBounds = GetCandidateTileCoordinateBounds(sweepBounds);

      TileMapCollisionResult horizontalBasicTileCollisionResult = null;

      if (objectMovementOffset.X > 0) {
        horizontalBasicTileCollisionResult = TestRightMovementCollision(objectBounds, objectMovementOffset, candidateTileIndexBounds);
      }
      else if (objectMovementOffset.X < 0) {
        horizontalBasicTileCollisionResult = TestLeftMovementCollision(objectBounds, objectMovementOffset, candidateTileIndexBounds);
      }

      if (horizontalBasicTileCollisionResult == null) {
        horizontalBasicTileCollisionResult = new TileMapCollisionResult {
          HasCollided = false,
          UncollidingMovementOffset = objectMovementOffset
        };
      }


      if (horizontalBasicTileCollisionResult.HasCollided) {
        objectMovementOffset = horizontalBasicTileCollisionResult.UncollidingMovementOffset;
        newTargetBounds = objectBounds.Shift(objectMovementOffset);
        sweepBounds = Rectangle.Union(objectBounds, newTargetBounds);
        candidateTileIndexBounds = GetCandidateTileCoordinateBounds(sweepBounds);
      }



      TileMapCollisionResult verticalBasicTileCollisionResult = null;

      if (objectMovementOffset.Y > 0) {
        verticalBasicTileCollisionResult = TestDownMovementCollision(objectBounds, objectMovementOffset, candidateTileIndexBounds);
      }
      else if (objectMovementOffset.Y < 0) {
        verticalBasicTileCollisionResult = TestUpMovementCollision(objectBounds, objectMovementOffset, candidateTileIndexBounds);
      }

      if (verticalBasicTileCollisionResult == null) {
        verticalBasicTileCollisionResult = new TileMapCollisionResult {
          HasCollided = false,
          UncollidingMovementOffset = objectMovementOffset
        };
      }


      var collisionResult = new TileMapCollisionResult();
      collisionResult.HasCollided = verticalBasicTileCollisionResult.HasCollided || horizontalBasicTileCollisionResult.HasCollided;
      collisionResult.CollisionOnBottom = verticalBasicTileCollisionResult.CollisionOnBottom;
      collisionResult.CollisionOnTop = verticalBasicTileCollisionResult.CollisionOnTop;
      collisionResult.CollisionOnLeft = horizontalBasicTileCollisionResult.CollisionOnLeft;
      collisionResult.CollisionOnRight = horizontalBasicTileCollisionResult.CollisionOnRight;
      collisionResult.UncollidingMovementOffset = verticalBasicTileCollisionResult.UncollidingMovementOffset;
      collisionResult.TileCollisionIndices.AddRange(horizontalBasicTileCollisionResult.TileCollisionIndices);
      collisionResult.TileCollisionIndices.AddRange(verticalBasicTileCollisionResult.TileCollisionIndices);
      collisionResult.OriginalOffset = originalOffset;
      return collisionResult;
    }

    private TileMapCollisionResult TestDownMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds) {

      int yOffset = objectMovementOffset.Y;
      Point collisionTileIndex = new Point(0, 0);

      for (int yTileIndex = candidateTileIndexBounds.Top; yTileIndex <= candidateTileIndexBounds.Bottom; yTileIndex++) {
        for (int xTileIndex = candidateTileIndexBounds.Left; xTileIndex <= candidateTileIndexBounds.Right; xTileIndex++) {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (!ObstacleMap[xTileIndex][yTileIndex] && !PlatformMap[xTileIndex][yTileIndex]) continue;

          // If the tile above is also solid, don't test collision on this edge;
          if (yTileIndex > 0 && ObstacleMap[xTileIndex][yTileIndex - 1]) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's bottom edge is actually moving throught
          // the top Y edge of the tile
          if (tileBounds.Top < objectBounds.Bottom ||
              tileBounds.Top > objectBounds.Bottom + yOffset) {
            continue;
          }

          int previousYOffset = yOffset;
          yOffset = Math.Min((tileBounds.Top) - objectBounds.Bottom, yOffset);
          //if (yOffset < 0) yOffset = 0;

          if (previousYOffset != yOffset) {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (yOffset != objectMovementOffset.Y) {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnBottom = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(objectMovementOffset.X, yOffset);
      }
      else {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }

    private TileMapCollisionResult TestUpMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds) {
      int yOffset = objectMovementOffset.Y;
      Point collisionTileIndex = new Point(0, 0);

      for (int yTileIndex = candidateTileIndexBounds.Bottom; yTileIndex >= candidateTileIndexBounds.Top; yTileIndex--) {
        for (int xTileIndex = candidateTileIndexBounds.Left; xTileIndex <= candidateTileIndexBounds.Right; xTileIndex++) {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (!ObstacleMap[xTileIndex][yTileIndex]) continue;

          // If the tile above is also solid, don't test collision on this edge;
          if ((yTileIndex + 1) < HeightInTiles && ObstacleMap[xTileIndex][yTileIndex + 1]) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's top edge is actually moving throught
          // the bottom Y edge of the tile
          if (tileBounds.Bottom > objectBounds.Top ||
              tileBounds.Bottom < objectBounds.Top + yOffset) {
            continue;
          }

          int previousYOffset = yOffset;
          yOffset = Math.Max((tileBounds.Bottom) - objectBounds.Top, yOffset);
          //if (yOffset > 0) yOffset = 0;

          if (previousYOffset != yOffset) {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (yOffset != objectMovementOffset.Y) {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnTop = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(objectMovementOffset.X, yOffset);
      }
      else {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }

    private TileMapCollisionResult TestRightMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds) {

      int xOffset = objectMovementOffset.X;
      Point collisionTileIndex = new Point(0, 0);

      for (int xTileIndex = candidateTileIndexBounds.Left; xTileIndex <= candidateTileIndexBounds.Right; xTileIndex++) {
        for (int yTileIndex = candidateTileIndexBounds.Top; yTileIndex <= candidateTileIndexBounds.Bottom; yTileIndex++) {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (!ObstacleMap[xTileIndex][yTileIndex]) continue;

          // If the tile to the right is also solid, don't test collision on this edge;
          if (xTileIndex > 0 && ObstacleMap[xTileIndex - 1][yTileIndex]) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's bottom edge is actually moving throught
          // the top Y edge of the tile
          if (tileBounds.Left < objectBounds.Right ||
              tileBounds.Left > objectBounds.Right + xOffset) {
            continue;
          }

          int previousXOffset = xOffset;
          xOffset = Math.Min((tileBounds.Left) - objectBounds.Right, xOffset);
          //if (xOffset < 0) xOffset = 0;

          if (previousXOffset != xOffset) {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (xOffset != objectMovementOffset.X) {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnRight = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(xOffset, objectMovementOffset.Y);
      }
      else {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }

    private TileMapCollisionResult TestLeftMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds) {

      int xOffset = objectMovementOffset.X;
      Point collisionTileIndex = new Point(0, 0);

      for (int xTileIndex = candidateTileIndexBounds.Right; xTileIndex >= candidateTileIndexBounds.Left; xTileIndex--) {
        for (int yTileIndex = candidateTileIndexBounds.Top; yTileIndex <= candidateTileIndexBounds.Bottom; yTileIndex++) {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (!ObstacleMap[xTileIndex][yTileIndex]) continue;

          // If the tile to the left is also solid, don't test collision on this edge;
          if ((xTileIndex < (WidthInTiles - 1)) && ObstacleMap[xTileIndex + 1][yTileIndex]) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's lefft edge is actually moving throught
          // the right edge of the tile
          if (tileBounds.Right > objectBounds.Left ||
              tileBounds.Right < objectBounds.Left + xOffset) {
            continue;
          }

          int previousXOffset = xOffset;
          xOffset = Math.Max((tileBounds.Right) - objectBounds.Left, xOffset);
          //if (xOffset > 0) xOffset = 0;

          if (previousXOffset != xOffset) {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (xOffset != objectMovementOffset.X) {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnLeft = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(xOffset, objectMovementOffset.Y);
      }
      else {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }


    private Rectangle GetTileBounds(int xTileIndex, int yTileIndex) {
      return new Rectangle(xTileIndex * TileWidth + Position.X, yTileIndex * TileHeight + Position.Y, TileWidth, TileHeight);
    }

    private Rectangle GetCandidateTileCoordinateBounds(Rectangle sweepBounds) {
      Point topLeft = GetTileCoordinateOf(new Point(sweepBounds.Left, sweepBounds.Top));
      Point bottomRight = GetTileCoordinateOf(new Point(sweepBounds.Right - 1, sweepBounds.Bottom - 1));
      return new Rectangle(topLeft.X, topLeft.Y, bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y);
    }

    private Point GetTileCoordinateOf(Point point) {
      var normalizedPoint = point.Shift(-Position.X, -Position.Y);
      return new Point(normalizedPoint.X / TileWidth, normalizedPoint.Y / TileHeight);
    }
  }
}
