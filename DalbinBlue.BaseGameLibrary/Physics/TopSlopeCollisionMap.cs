﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Physics
{
  public class TopSlopeCollisionMap : ICollisionMap {
    private readonly bool _pushIfOverLedge;

    private static readonly TopSlopeTileCollisionModel EmptyTile =
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty,
                                     TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, 
                                     Point.Zero, Point.Zero, false);

    private static readonly TopSlopeTileCollisionModel[] TileModelPrototypes = new[]
    {
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, Point.Zero, Point.Zero, false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, new Point(0,2), new Point(2,0), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, new Point(0,0), new Point(2,2), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, Point.Zero, Point.Zero, false),

      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, TileCollisionEdgeType.Complex, new Point(0,2), new Point(2,1), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, new Point(0,1), new Point(2,0), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, new Point(0,0), new Point(2,1), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, TileCollisionEdgeType.Complex, new Point(0,1), new Point(2,2), false),

      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, new Point(1,2), new Point(2,0), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, new Point(0,0), new Point(1,2), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(1,2), new Point(2,0), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,0), new Point(1,2), true),

      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, new Point(0,2), new Point(1,0), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Solid, TileCollisionEdgeType.Complex, new Point(1,0), new Point(2,2), false),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,2), new Point(1,0), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(1,0), new Point(2,2), true),

      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Solid, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, Point.Zero, Point.Zero, true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,2), new Point(2,0), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,0), new Point(2,2), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, Point.Zero, Point.Zero, true),

      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,2), new Point(2,1), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,1), new Point(2,0), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,0), new Point(2,1), true),
      new TopSlopeTileCollisionModel(TileCollisionEdgeType.Complex, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, TileCollisionEdgeType.Empty, new Point(0,1), new Point(2,2), true),
    };

    public int WidthInTiles { get; protected set; }
    public int HeightInTiles { get; protected set; }
    public int TileWidth { get; protected set; }
    public int TileHeight { get; protected set; }
    public Point Position { get; set; }

    private readonly TopSlopeTileCollisionModel[][] _collisionModel;

    private TopSlopeCollisionMap(int widthInTiles, int heightInTiles, int tileWidth, int tileHeight, bool pushIfOverLedge) {
      _pushIfOverLedge = pushIfOverLedge;
      WidthInTiles = widthInTiles;
      HeightInTiles = heightInTiles;
      TileWidth = tileWidth;
      TileHeight = tileHeight;
      Position = new Point(0, 0);
      _collisionModel = new TopSlopeTileCollisionModel[widthInTiles][];
      for (int i = 0; i < widthInTiles; i++) {
        _collisionModel[i] = new TopSlopeTileCollisionModel[heightInTiles];
      }
    }

    public static TopSlopeCollisionMap FromTileMapLayer(TileMap tileMap, int layerIndex, bool pushIfOverLedge) {
      var map = new TopSlopeCollisionMap(tileMap.WidthInTiles, tileMap.HeightInTiles, tileMap.TileWidth, tileMap.TileHeight, pushIfOverLedge);
      var layer = tileMap.Layers[layerIndex];

      // First pass generation of tile model

      for (int x = 0; x < tileMap.WidthInTiles; x++) {
        for (int y = 0; y < tileMap.HeightInTiles; y++) {
          long index = layer.LayerTiles[x][y];

          if (index == 0) {
            map._collisionModel[x][y] = new TopSlopeTileCollisionModel(EmptyTile);
            continue;
          }

          var tileSet =
            tileMap.Tilesets.Where(ts => ts.FirstTileId <= index).OrderByDescending(ts => ts.FirstTileId).First();
          index -= tileSet.FirstTileId;

          map._collisionModel[x][y] = new TopSlopeTileCollisionModel(TileModelPrototypes[index]);
        }
      }

      // Second pass optimization

      for (int x = 0; x < tileMap.WidthInTiles; x++) {
        for (int y = 0; y < tileMap.HeightInTiles; y++) {
          // Optimize bottom edge

          if ((y + 1 < tileMap.HeightInTiles) && map._collisionModel[x][y].BottomEdge == TileCollisionEdgeType.Solid
              && map._collisionModel[x][y + 1].TopEdge == TileCollisionEdgeType.Solid) {
            map._collisionModel[x][y].BottomEdge = TileCollisionEdgeType.Empty;
            map._collisionModel[x][y + 1].TopEdge = TileCollisionEdgeType.Empty;
          }

          // Optimize right edge
          if ((x + 1 < tileMap.WidthInTiles) && map._collisionModel[x][y].RightEdge == TileCollisionEdgeType.Solid
              && map._collisionModel[x + 1][y].LeftEdge == TileCollisionEdgeType.Solid)
          {
            map._collisionModel[x][y].RightEdge = TileCollisionEdgeType.Empty;
            map._collisionModel[x + 1][y].LeftEdge = TileCollisionEdgeType.Empty;
          }        
        }
      }
      
      return map;
    }

    public bool IsObstacleAtTilePosition(Point tilePosition) {
      var tile = _collisionModel[tilePosition.X][tilePosition.Y];
      return tile.BottomEdge != TileCollisionEdgeType.Empty 
        && tile.LeftEdge != TileCollisionEdgeType.Empty 
        && tile.RightEdge != TileCollisionEdgeType.Empty 
        && tile.BottomEdge != TileCollisionEdgeType.Empty;
    }

    public TileMapCollisionResult TestCollision(Rectangle objectBounds, Point objectMovementOffset, bool onGroundPreviously) {
      var originalOffset = objectMovementOffset;

      // Get the entire rectange that the object sweeps through
      var newTargetBounds = objectBounds.Shift(objectMovementOffset);
      var sweepBounds = Rectangle.Union(objectBounds, newTargetBounds);

      // Determine the range of x and y indices that the player is moving through
      Rectangle candidateTileIndexBounds = GetCandidateTileCoordinateBounds(sweepBounds);



      var horizontalBasicTileCollisionResult = TestHorizontalCollision(objectBounds, ref objectMovementOffset, ref candidateTileIndexBounds);

      TileMapCollisionResult verticalBasicTileCollisionResult = null;

      if (objectMovementOffset.Y > 0)
      {
        verticalBasicTileCollisionResult = TestDownMovementCollision(objectBounds, objectMovementOffset, candidateTileIndexBounds);
          
        if (verticalBasicTileCollisionResult.CollisionOnBottom && verticalBasicTileCollisionResult.OnPlatform) {
          var additionalResult =
            TestCollision(objectBounds.Shift(new Point(0,verticalBasicTileCollisionResult.UncollidingMovementOffset.Y)),
                          new Point(originalOffset.X, 0), onGroundPreviously);
          if (additionalResult.HasCollided) {
            horizontalBasicTileCollisionResult = additionalResult;
          }
        }
        else if (onGroundPreviously && verticalBasicTileCollisionResult.CollisionOnBottom == false)
        {
          var previousOnGroundOffset = objectMovementOffset.Shift(0, 10);
          var previousOnGroundNewTargetBounds = objectBounds.Shift(previousOnGroundOffset);
          var previousOnGroundSweepBounds = Rectangle.Union(objectBounds, previousOnGroundNewTargetBounds);

          // Determine the range of x and y indices that the player is moving through
          Rectangle previousOnGroundCandidateTileIndexBounds = GetCandidateTileCoordinateBounds(previousOnGroundSweepBounds);

          var previouslyOnGroundResult = TestDownMovementCollision(objectBounds, previousOnGroundOffset, previousOnGroundCandidateTileIndexBounds);
          if (previouslyOnGroundResult.CollisionOnBottom == true)
          {
            verticalBasicTileCollisionResult = previouslyOnGroundResult;
          }
        }
      }
      else if (objectMovementOffset.Y < 0)
      {
        verticalBasicTileCollisionResult = TestUpMovementCollision(objectBounds, objectMovementOffset, candidateTileIndexBounds);
      }

      if (verticalBasicTileCollisionResult == null)
      {
        verticalBasicTileCollisionResult = new TileMapCollisionResult
        {
          HasCollided = false,
          UncollidingMovementOffset = objectMovementOffset
        };
      }

      var collisionResult = new TileMapCollisionResult();
      collisionResult.HasCollided = verticalBasicTileCollisionResult.HasCollided || horizontalBasicTileCollisionResult.HasCollided;
      collisionResult.CollisionOnBottom = verticalBasicTileCollisionResult.CollisionOnBottom;
      collisionResult.CollisionOnTop = verticalBasicTileCollisionResult.CollisionOnTop;
      collisionResult.CollisionOnLeft = horizontalBasicTileCollisionResult.CollisionOnLeft;
      collisionResult.CollisionOnRight = horizontalBasicTileCollisionResult.CollisionOnRight;
      collisionResult.UncollidingMovementOffset = verticalBasicTileCollisionResult.UncollidingMovementOffset;
      collisionResult.TileCollisionIndices.AddRange(horizontalBasicTileCollisionResult.TileCollisionIndices);
      collisionResult.TileCollisionIndices.AddRange(verticalBasicTileCollisionResult.TileCollisionIndices);
      collisionResult.OriginalOffset = originalOffset;
      collisionResult.OnPlatform = verticalBasicTileCollisionResult.OnPlatform;
      return collisionResult;
    }

    private TileMapCollisionResult TestHorizontalCollision(
      Rectangle objectBounds, ref Point objectMovementOffset, ref Rectangle candidateTileIndexBounds) {
      Rectangle newTargetBounds;
      Rectangle sweepBounds;
      TileMapCollisionResult horizontalBasicTileCollisionResult = null;

      if (objectMovementOffset.X > 0) {
        horizontalBasicTileCollisionResult = TestRightMovementCollision(objectBounds, objectMovementOffset,
                                                                        candidateTileIndexBounds);
      }
      else if (objectMovementOffset.X < 0) {
        horizontalBasicTileCollisionResult = TestLeftMovementCollision(objectBounds, objectMovementOffset,
                                                                       candidateTileIndexBounds);
      }

      if (horizontalBasicTileCollisionResult == null) {
        horizontalBasicTileCollisionResult = new TileMapCollisionResult {
          HasCollided = false,
          UncollidingMovementOffset = objectMovementOffset
        };
      }


      if (horizontalBasicTileCollisionResult.HasCollided) {
        objectMovementOffset = horizontalBasicTileCollisionResult.UncollidingMovementOffset;
        newTargetBounds = objectBounds.Shift(objectMovementOffset);
        sweepBounds = Rectangle.Union(objectBounds, newTargetBounds);
        candidateTileIndexBounds = GetCandidateTileCoordinateBounds(sweepBounds);
      }
      return horizontalBasicTileCollisionResult;
    }


    private TileMapCollisionResult TestRightMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds)
    {

      int xOffset = objectMovementOffset.X;
      Point collisionTileIndex = new Point(0, 0);

      for (int xTileIndex = candidateTileIndexBounds.Left; xTileIndex <= candidateTileIndexBounds.Right; xTileIndex++)
      {
        for (int yTileIndex = candidateTileIndexBounds.Top; yTileIndex <= candidateTileIndexBounds.Bottom; yTileIndex++)
        {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (_collisionModel[xTileIndex][yTileIndex].LeftEdge == TileCollisionEdgeType.Empty) continue;

          //TODO Fix This
          if (_collisionModel[xTileIndex][yTileIndex].LeftEdge == TileCollisionEdgeType.Complex) continue;


          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's bottom edge is actually moving throught
          // the top Y edge of the tile
          if (tileBounds.Left < objectBounds.Right ||
              tileBounds.Left > objectBounds.Right + xOffset)
          {
            continue;
          }

          int previousXOffset = xOffset;
          xOffset = Math.Min((tileBounds.Left) - objectBounds.Right, xOffset);
          //if (xOffset < 0) xOffset = 0;

          if (previousXOffset != xOffset)
          {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (xOffset != objectMovementOffset.X)
      {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnRight = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(xOffset, objectMovementOffset.Y);
      }
      else
      {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }

    private TileMapCollisionResult TestLeftMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds)
    {

      int xOffset = objectMovementOffset.X;
      Point collisionTileIndex = new Point(0, 0);

      for (int xTileIndex = candidateTileIndexBounds.Right; xTileIndex >= candidateTileIndexBounds.Left; xTileIndex--)
      {
        for (int yTileIndex = candidateTileIndexBounds.Top; yTileIndex <= candidateTileIndexBounds.Bottom; yTileIndex++)
        {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (_collisionModel[xTileIndex][yTileIndex].RightEdge == TileCollisionEdgeType.Empty) continue;

          //TODO FIX THIS
          if (_collisionModel[xTileIndex][yTileIndex].RightEdge == TileCollisionEdgeType.Complex) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's lefft edge is actually moving throught
          // the right edge of the tile
          if (tileBounds.Right > objectBounds.Left ||
              tileBounds.Right < objectBounds.Left + xOffset)
          {
            continue;
          }

          int previousXOffset = xOffset;
          xOffset = Math.Max((tileBounds.Right) - objectBounds.Left, xOffset);
          //if (xOffset > 0) xOffset = 0;

          if (previousXOffset != xOffset)
          {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (xOffset != objectMovementOffset.X)
      {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnLeft = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(xOffset, objectMovementOffset.Y);
      }
      else
      {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }


    private TileMapCollisionResult TestDownMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds) {
      int xOffset = objectMovementOffset.X;
      int yOffset = objectMovementOffset.Y;
      Point collisionTileIndex = new Point(0, 0);

      for (int yTileIndex = candidateTileIndexBounds.Top; yTileIndex <= candidateTileIndexBounds.Bottom; yTileIndex++)
      {
        for (int xTileIndex = candidateTileIndexBounds.Left; xTileIndex <= candidateTileIndexBounds.Right; xTileIndex++)
        {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          var tileCollisionModel = _collisionModel[xTileIndex][yTileIndex];
          if (tileCollisionModel.TopEdge == TileCollisionEdgeType.Empty) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          if (tileCollisionModel.TopEdge == TileCollisionEdgeType.Solid) {

            // Make sure the object's bottom edge is actually moving throught
            // the top Y edge of the tile
            if (tileBounds.Top < objectBounds.Bottom ||
                tileBounds.Top > objectBounds.Bottom + yOffset) {
              continue;
            }

            int previousYOffset = yOffset;
            yOffset = Math.Min((tileBounds.Top) - objectBounds.Bottom, yOffset);

            if (previousYOffset != yOffset) {
              collisionTileIndex = new Point(xTileIndex, yTileIndex);
            }
          }
          else {
            var slopeStart = new Point(tileCollisionModel.TopSlopeStart.X * TileWidth / 2,
                                       tileCollisionModel.TopSlopeStart.Y * TileHeight / 2);
            var slopeEnd = new Point(tileCollisionModel.TopSlopeEnd.X * TileWidth / 2,
                                       tileCollisionModel.TopSlopeEnd.Y * TileHeight / 2);

            var bottomCenterCoordinate = GetBottomCenterCoordinate(objectBounds, objectMovementOffset);

            bottomCenterCoordinate = bottomCenterCoordinate.Shift(-tileBounds.Left, -tileBounds.Top);

            if (bottomCenterCoordinate.X > slopeStart.X &&
                bottomCenterCoordinate.X < slopeEnd.X) {

              if (objectBounds.Bottom > tileBounds.Bottom) {
                continue;
              }

              var slopeY = (slopeEnd.Y - slopeStart.Y)*(bottomCenterCoordinate.X - slopeStart.X)/
                           (slopeEnd.X - slopeStart.X) + slopeStart.Y;

              int previousYOffset = yOffset;
              yOffset = Math.Min((tileBounds.Top + slopeY) - objectBounds.Bottom, yOffset);

              //yOffset = Math.Min((slopeY) - bottomCenterCoordinate.Y, yOffset);

              if (previousYOffset != yOffset) {
                collisionTileIndex = new Point(xTileIndex, yTileIndex);
              }
            }
            else {
              if (tileBounds.Bottom < objectBounds.Bottom)
              {
                continue;
              }

              Point higherEdge;
              bool higherEdgeOnLeft;
              Point lowerEdge;
              Point lowerEdgeUnscaled;
              

              if (slopeStart.Y < slopeEnd.Y) {
                higherEdge = slopeStart;
                higherEdgeOnLeft = true;
                lowerEdge = slopeEnd;
                lowerEdgeUnscaled = tileCollisionModel.TopSlopeEnd;
              }
              else {
                higherEdge = slopeEnd;
                higherEdgeOnLeft = false;
                lowerEdge = slopeStart;
                lowerEdgeUnscaled = tileCollisionModel.TopSlopeStart;
              }

              higherEdge = higherEdge.Shift(tileBounds.Left, tileBounds.Top);
              lowerEdge = lowerEdge.Shift(tileBounds.Left, tileBounds.Top);

              if ((higherEdgeOnLeft && higherEdge.X >= objectBounds.Center.X + objectMovementOffset.X && higherEdge.X <= objectBounds.Right + objectMovementOffset.X)
                || (!higherEdgeOnLeft && higherEdge.X >= objectBounds.Left + objectMovementOffset.X && higherEdge.X <= objectBounds.Center.X + objectMovementOffset.X)) {


                int previousYOffset = yOffset;
                yOffset = Math.Min((higherEdge.Y) - objectBounds.Bottom, yOffset);

                if (previousYOffset != yOffset) {
                  collisionTileIndex = new Point(xTileIndex, yTileIndex);
                }
              }
              else if (lowerEdge.X >= objectBounds.Left + objectMovementOffset.X &&
                       lowerEdge.X <= objectBounds.Right + objectBounds.X) {

                if ((lowerEdgeUnscaled.X == 0)
                    && (xTileIndex > 0)
                    && (_collisionModel[xTileIndex - 1][yTileIndex].RightEdge == TileCollisionEdgeType.Empty)
                    && ((yTileIndex == HeightInTiles - 1)
                        || ((lowerEdgeUnscaled.Y==2) && (_collisionModel[xTileIndex - 1][yTileIndex + 1].TopEdge == TileCollisionEdgeType.Empty))
                        || (_collisionModel[xTileIndex - 1][yTileIndex + 1].TopEdge == TileCollisionEdgeType.Complex
                            && ((lowerEdgeUnscaled.Y!=2 || _collisionModel[xTileIndex - 1][yTileIndex + 1].TopSlopeEnd != new Point(2, 0))
                              && (lowerEdgeUnscaled.Y!=1 || _collisionModel[xTileIndex - 1][yTileIndex].TopSlopeEnd != new Point(2, 1)))))) {

                  int previousYOffset = yOffset;
                  yOffset = Math.Min((lowerEdge.Y - 1) - objectBounds.Bottom, yOffset);

                  if (_pushIfOverLedge) {
                    xOffset = objectMovementOffset.X - objectBounds.Width/8;
                  }

                  if (previousYOffset != yOffset)
                  {
                      collisionTileIndex = new Point(xTileIndex, yTileIndex);                      
                  }
                }
                else if ((lowerEdgeUnscaled.X == 2)
                         && (xTileIndex < WidthInTiles - 1)
                         && (_collisionModel[xTileIndex + 1][yTileIndex].LeftEdge == TileCollisionEdgeType.Empty)
                         && ((yTileIndex == HeightInTiles - 1)
                             || ((lowerEdgeUnscaled.Y == 2) && (_collisionModel[xTileIndex + 1][yTileIndex + 1].TopEdge == TileCollisionEdgeType.Empty))
                             || (_collisionModel[xTileIndex + 1][yTileIndex + 1].TopEdge == TileCollisionEdgeType.Complex
                                && ((lowerEdgeUnscaled.Y != 2 || _collisionModel[xTileIndex + 1][yTileIndex + 1].TopSlopeStart != new Point(0, 0))
                                  && (lowerEdgeUnscaled.Y != 1 || _collisionModel[xTileIndex + 1][yTileIndex].TopSlopeStart != new Point(0, 1)))))) {


                  int previousYOffset = yOffset;
                  yOffset = Math.Min((lowerEdge.Y - 1) - objectBounds.Bottom, yOffset);

                  if (_pushIfOverLedge) {
                    xOffset = objectMovementOffset.X + objectBounds.Width/8;
                  }

                  if (previousYOffset != yOffset) {
                    collisionTileIndex = new Point(xTileIndex, yTileIndex);
                  }
                }
              }
            }
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (yOffset != objectMovementOffset.Y)
      {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnBottom = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(xOffset, yOffset);
        collisionResult.OnPlatform = _collisionModel[collisionTileIndex.X][collisionTileIndex.Y].IsPlatform;
      }
      else
      {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }

    private Point GetBottomCenterCoordinate(Rectangle objectBounds, Point objectMovementOffset) {
      return new Point(
          objectBounds.Center.X + objectMovementOffset.X,
          objectBounds.Bottom + objectMovementOffset.Y
        );
    }

    private TileMapCollisionResult TestUpMovementCollision(Rectangle objectBounds, Point objectMovementOffset, Rectangle candidateTileIndexBounds)
    {
      int yOffset = objectMovementOffset.Y;
      Point collisionTileIndex = new Point(0, 0);

      for (int yTileIndex = candidateTileIndexBounds.Bottom; yTileIndex >= candidateTileIndexBounds.Top; yTileIndex--)
      {
        for (int xTileIndex = candidateTileIndexBounds.Left; xTileIndex <= candidateTileIndexBounds.Right; xTileIndex++)
        {
          if (yTileIndex < 0 || yTileIndex >= HeightInTiles || xTileIndex < 0 || xTileIndex >= WidthInTiles) continue;

          if (_collisionModel[xTileIndex][yTileIndex].BottomEdge == TileCollisionEdgeType.Empty) continue;

          var tileBounds = GetTileBounds(xTileIndex, yTileIndex);

          // Make sure the object's top edge is actually moving throught
          // the bottom Y edge of the tile
          if (tileBounds.Bottom > objectBounds.Top ||
              tileBounds.Bottom < objectBounds.Top + yOffset)
          {
            continue;
          }

          int previousYOffset = yOffset;
          yOffset = Math.Max((tileBounds.Bottom) - objectBounds.Top, yOffset);
          //if (yOffset > 0) yOffset = 0;

          if (previousYOffset != yOffset)
          {
            collisionTileIndex = new Point(xTileIndex, yTileIndex);
          }
        }
      }

      var collisionResult = new TileMapCollisionResult();

      if (yOffset != objectMovementOffset.Y)
      {
        collisionResult.HasCollided = true;
        collisionResult.CollisionOnTop = true;
        collisionResult.TileCollisionIndices.Add(collisionTileIndex);
        collisionResult.UncollidingMovementOffset = new Point(objectMovementOffset.X, yOffset);
      }
      else
      {
        collisionResult.HasCollided = false;
        collisionResult.UncollidingMovementOffset = objectMovementOffset;
      }

      return collisionResult;
    }


    //TODO MOVE TO COMMON AREA
    private Rectangle GetCandidateTileCoordinateBounds(Rectangle sweepBounds)
    {
      Point topLeft = GetTileCoordinateOf(new Point(sweepBounds.Left, sweepBounds.Top));
      Point bottomRight = GetTileCoordinateOf(new Point(sweepBounds.Right - 1, sweepBounds.Bottom - 1));
      return new Rectangle(topLeft.X, topLeft.Y, bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y);
    }

    //TODO MOVE TO COMMON AREA
    private Point GetTileCoordinateOf(Point point)
    {
      var normalizedPoint = point.Shift(-Position.X, -Position.Y);
      return new Point(normalizedPoint.X / TileWidth, normalizedPoint.Y / TileHeight);
    }

    //TODO MOVE TO COMMON AREA
    private Rectangle GetTileBounds(int xTileIndex, int yTileIndex)
    {
      return new Rectangle(xTileIndex * TileWidth + Position.X, yTileIndex * TileHeight + Position.Y, TileWidth, TileHeight);
    }

    private struct TopSlopeTileCollisionModel {
      public TopSlopeTileCollisionModel(
        TileCollisionEdgeType topEdge, TileCollisionEdgeType bottomEdge, TileCollisionEdgeType leftEdge,
        TileCollisionEdgeType rightEdge, Point topSlopeStart, Point topSlopeEnd, bool isPlatform)
        : this() {
        IsPlatform = isPlatform;
        TopEdge = topEdge;
        BottomEdge = bottomEdge;
        LeftEdge = leftEdge;
        RightEdge = rightEdge;
        TopSlopeStart = topSlopeStart;
        TopSlopeEnd = topSlopeEnd;
      }

      public TopSlopeTileCollisionModel(TopSlopeTileCollisionModel value)
        : this() {
        TopEdge = value.TopEdge;
        BottomEdge = value.BottomEdge;
        LeftEdge = value.LeftEdge;
        RightEdge = value.RightEdge;
        TopSlopeStart = value.TopSlopeStart;
        TopSlopeEnd = value.TopSlopeEnd;
        IsPlatform = value.IsPlatform;
      }

      public TileCollisionEdgeType TopEdge { get; set; }
      public TileCollisionEdgeType BottomEdge { get; set; }
      public TileCollisionEdgeType LeftEdge { get; set; }
      public TileCollisionEdgeType RightEdge { get; set; }
      public Point TopSlopeStart { get; set; }
      public Point TopSlopeEnd { get; set; }
      public bool IsPlatform { get; set; }

    }
  }
}
