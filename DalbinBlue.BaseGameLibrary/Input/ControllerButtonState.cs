﻿using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Input {
  public class ControllerButtonState {
    public ControllerButtonPosition Position { get; set; }
    public GameTime TimeOfPress { get; set; }
    public double SecondsSinceButtonPressed { get; set; }
    public double SecondsSinceLastRepeat { get; set; }
    public int RepeatCount { get; set; }
    public bool IsDown { get { return (Position & ControllerButtonPosition.Down) != ControllerButtonPosition.None; } }
    public bool IsUp { get { return (Position & ControllerButtonPosition.Up) != ControllerButtonPosition.None; } }
    public bool IsJustPressed { get { return (Position & ControllerButtonPosition.JustPressed) != ControllerButtonPosition.None; } }
    public bool IsJustReleased { get { return (Position & ControllerButtonPosition.JustReleased) != ControllerButtonPosition.None; } }
    public bool IsDoubleTap { get; set; }
  }
}
