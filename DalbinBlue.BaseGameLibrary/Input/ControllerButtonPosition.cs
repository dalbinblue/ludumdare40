﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dalbinblue.BaseGameLibrary.Input {
  [Flags]
  public enum ControllerButtonPosition {
    None = 0,
    Up = 1,
    Down = 2,
    JustPressed = 4,
    JustReleased = 8
  }
}
