namespace Dalbinblue.BaseGameLibrary.Data.Map {
  public enum TileMapOrientation {
    Orthogonal,
    Isometric
  }
}