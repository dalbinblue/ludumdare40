namespace Dalbinblue.BaseGameLibrary.Data.Map {
  public enum TileMapObjectGeometryType {
    Tile,
    PolyLine,
    Polygon,
    Rectangle,
    Point
  }
}