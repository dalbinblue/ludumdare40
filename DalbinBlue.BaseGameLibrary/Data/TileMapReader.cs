﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Microsoft.Xna.Framework.Content;

namespace Dalbinblue.BaseGameLibrary.Data {
  public class TileMapReader : ContentTypeReader<TileMap> {
    protected override TileMap Read(ContentReader input, TileMap existingInstance) {
      int dataLength = input.ReadInt32();
      byte[] data = input.ReadBytes(dataLength);
      using (var stream = new MemoryStream(data)) {
        IFormatter binaryFormatter = new BinaryFormatter();
        var tileMap = (TileMap)binaryFormatter.Deserialize(stream);
        return tileMap;
      }
    }
  }
}
