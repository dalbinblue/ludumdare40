using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Sprite {
  [Serializable]
  public class SpriteDefinition
  {
    public string DefaultAnimation;
    public Dictionary<string, SpriteAnimationDefinition> Animations { get; set; }
  }
}