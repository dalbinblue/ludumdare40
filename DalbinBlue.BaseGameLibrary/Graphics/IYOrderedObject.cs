namespace Dalbinblue.BaseGameLibrary.Graphics
{
    public interface IYOrderedObject
    {
        float Bottom { get; }
    }
}