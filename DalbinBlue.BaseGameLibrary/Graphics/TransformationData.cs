using Microsoft.Xna.Framework;

public class TransformationData {
  public Vector2 Position { get; set; }
  public float Rotation { get; set; }
  public Vector2 Scale { get; set; }
}