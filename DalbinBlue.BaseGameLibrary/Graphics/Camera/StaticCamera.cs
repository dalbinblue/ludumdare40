using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics.Camera {
  public class StaticCamera : ICamera {
    public float Rotation { get; set; }
    public float Zoom { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public StaticCamera() {
      Rotation = 0;
      Zoom = 1;
      X = 0;
      Y = 0;
    }

    public void Update(GameTime gameTime) {
      // DO NOTHING
    }
  }
}