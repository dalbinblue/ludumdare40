using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public interface IRenderableObject : IPositionedObject {
    Vector2 Origin { get; }
    int Width { get; }
    int Height { get; }
    float Layer { get; }
    Rectangle GetBounds();
    bool RenderIfOutOfBounds { get; }
    void Update(GameTime gameTime);
    //void Draw(SpriteBatch spriteBatch);
    void Draw(SpriteBatch spriteBatch, Matrix parentTransformation);
  }
}