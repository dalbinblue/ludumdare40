namespace Dalbinblue.BaseGameLibrary.Audio
{
  public class MusicAssetOptions
  {
    public bool LoopMusic { get; set; }

    /// <summary>
    /// The name of the registered music that should automatically be played 
    /// when this song's playback has been completed.  If the music set to loop
    /// This option will be ignored.
    /// </summary>
    public string MusicToSwitchToWhenPlayingComplete { get; set; }

    public static readonly MusicAssetOptions DefaultMusicAssetOptions =
      new MusicAssetOptions 
      {
        LoopMusic = true,
        MusicToSwitchToWhenPlayingComplete = null
      };
  }
}