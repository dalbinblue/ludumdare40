namespace Dalbinblue.BaseGameLibrary.Audio
{
  internal class MusicItem
  {
    /// <summary>
    /// The name of the playable music item.  This is the key name used to 
    /// identify the music when requesting playback.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// The asset name for the actual song content from the content project.
    /// </summary>
    public string AssetName { get; set; }

    /// <summary>
    /// Additional detailed options for the playback of the music.
    /// </summary>
    public MusicAssetOptions Options { get; set; }
  }
}